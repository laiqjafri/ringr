#include <string.h>
#include <jni.h>
#include <android/log.h>

 extern "C" {
     JNIEXPORT jstring JNICALL Java_com_thirdwindow_ringr_RingrActivity_stringFromJNICPP(JNIEnv * env, jobject obj);
 };

 JNIEXPORT jstring JNICALL Java_com_thirdwindow_ringr_RingrActivity_stringFromJNICPP(JNIEnv * env, jobject obj)
 {
	 return env->NewStringUTF("HELLO Hassan");
 }
