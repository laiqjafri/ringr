LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := Ringr
LOCAL_SRC_FILES := Ringr.cpp

include $(BUILD_SHARED_LIBRARY)
